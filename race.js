(function() {

        window.addEventListener('load', load, false);
        var img1 = new Image(); // HTML5 Constructor

        function load() {
            img1.src = 'sanmarino12counter.png';

            CAAT.ModuleManager.
                baseURL("http://hyperandroid.github.io/CAAT/src/").
                setModulePath("CAAT.Core", "Core").
                setModulePath("CAAT.Math", "Math").
                setModulePath("CAAT.Behavior", "Behavior").
                setModulePath("CAAT.Foundation", "Foundation").
                setModulePath("CAAT.Event", "Event").
                setModulePath("CAAT.PathUtil", "PathUtil").
                setModulePath("CAAT.Module", "Modules").
                setModulePath("CAAT.Module.Preloader", "Modules/Image/Preloader").
                setModulePath("CAAT.WebGL", "WebGL").

                setModulePath("CAAT.FC", "../documentation/demos/demo20").

                // get modules, and solve their dependencies.
                bring([
                    "CAAT.PathUtil.Path",
                    "CAAT.PathUtil.ArcPath",
                    "CAAT.PathUtil.RectPath",
                    "CAAT.Foundation.Director",
                    "CAAT.Foundation.Actor",
                    "CAAT.Foundation.ActorContainer",
                    "CAAT.Foundation.UI.PathActor",
                    "CAAT.Foundation.UI.ShapeActor",
                    "CAAT.Math.Point",
                    "CAAT.FC.Ship",
                    "CAAT.Behavior.PathBehavior"
                ]).

                // this function will be fired every time all dependencies have been solved.
                // if you call again bring, this function could be fired again.
                onReady( __start );

        }

        function trackChangedCallback(path) {
            console.log(path);
        }


        function __start() {

            CAAT.DEBUG=1;

            var director= new CAAT.Foundation.Director().initialize(800, 600, "experiment-canvas");
            var scene= director.createScene();

            var path = new CAAT.Path().
            beginPath(383, 348).
            addCubicTo(381, 352, 659, 353, 664, 325).
            addCubicTo(703, 278, 667, 149, 700, 73).
            addCubicTo(675, 28, 544, 25, 439, 27).
            addCubicTo(356, 51, 326, 87, 290, 121).
            addCubicTo(275, 193, 291, 224, 381, 225).
            addCubicTo(421, 225, 442, 220, 482, 220).
            addCubicTo(500, 210, 516, 196, 490, 159).
            addCubicTo(445, 166, 401, 179, 395, 135).
            addCubicTo(416, 91, 452, 80, 557, 107).
            addCubicTo(598, 188, 632, 207, 607, 264).
            addCubicTo(582, 287, 576, 298, 530, 299).
            addCubicTo(411, 265, 272, 315, 215, 261).
            addCubicTo(190, 228, 207, 153, 256, 102).
            addCubicTo(252, 69, 251, 51, 194, 22).
            addCubicTo(139, 31, 125, 22, 59, 57).
            addCubicTo(24, 123, 26, 199, 35, 243).
            addCubicTo(52, 321, 114, 342, 158, 355).
            addCubicTo(273, 356, 306, 351, 378, 345).
            closePath();

            var pa = new CAAT.Foundation.UI.PathActor().
                    setBounds(0,0,800,600).
                    setInteractive(true).
                    setPath( path ).
                    setOnUpdateCallback(trackChangedCallback()).
                    setBackgroundImage(img1, true).
                    showBoundingBox(false);

            // doug
            var fish = new CAAT.Foundation.UI.ShapeActor().
            setSize(15, 15).
            enableEvents(false).
            setCompositeOp('lighter').
            setFillStyle('red');
            var pb = new CAAT.PathBehavior().
            setPath(path).
            setFrameTime(0, 5751).
            setCycle(true).
            setAutoRotate(true, CAAT.PathBehavior.autorotate.LEFT_TO_RIGHT).
            setTranslation(fish.width / 2, fish.height / 2);

            // vinicio
            var fish2 = new CAAT.Foundation.UI.ShapeActor().
            setSize(15, 15).
            enableEvents(false).
            setCompositeOp('lighter').
            setFillStyle('blue');
            var pb2 = new CAAT.PathBehavior().
            setPath(path).
            setFrameTime(0, 5750).
            setCycle(true).
            setAutoRotate(true, CAAT.PathBehavior.autorotate.LEFT_TO_RIGHT).
            setTranslation(fish.width / 2, fish.height / 2);

            //vivo
            var fish3 = new CAAT.Foundation.UI.ShapeActor().
            setSize(15, 15).
            enableEvents(false).
            setCompositeOp('lighter').
            setFillStyle('yellow');
            var pb3 = new CAAT.PathBehavior().
            setPath(path).
            setFrameTime(0, 5762).
            setCycle(true).
            setAutoRotate(true, CAAT.PathBehavior.autorotate.LEFT_TO_RIGHT).
            setTranslation(fish.width / 2, fish.height / 2);

            fish.addBehavior(pb);
            fish2.addBehavior(pb2);
            fish3.addBehavior(pb3);

            scene.addChild( pa );
            scene.addChild(fish);
            scene.addChild(fish2);
            scene.addChild(fish3);

            CAAT.loop(1);

        }
    })();